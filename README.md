***Shopify Recruitment App***

**Notes:**


 - It was not possible to enable Person Accounts in the developer edition environment. A true solution for separating Applicants and Internal Department Accounts would have utilized Person Accounts
 - A mock version of Person Accounts was created by Adding a Contact Lookup to Applicant Accounts. Users are blocked from creating Applicant Account records directly. They must create a Contact of Record Type Applicant. A trigger automatically creates the associated Applicant Account.
 - No Apex Tests were created, ideally all code created would be tested but it's very late on a Saturday :)
 - I purposefully avoided using Process Builder. It tends to perform far slower than Apex code and in my opinion more difficult to debug.  
 
 **Profiles:**
 

 - Very little info to create appropriate Profiles
 - Just one was created called 'Recruitment App' please log in and test as a User with this profile
 - System admin will be able to edit Read only fields that the 'Recruitment App' Profile will not, please bare that in mind
 - A Permission Set was created to enable access to Salary fields, please add this to your User for testing
 - Permission set was used as not much profile information so multiple profiles felt like overkill

 **Application:**
 

 - A Console Application was created for the purpose of this excercise. It is set as default, it's called 'Recruitment Cloud', please ensure you use this.

**REST API for Applications:**

 - Please see below an image of testing via workbench, you can test using any REST utility, workbench is easy. 
 - ![picture](https://bitbucket.org/eoghanmcm/recruitmentapplication/raw/a967a26f667121cb4de03e1fb14823cce6bb5fc4/REST%20Endpoint%20Workbench.PNG)

 - There is a json called applicationJsonExample.json in the Master folder. Please use it to see how JSON is structure
 - The Apex class is called: ShopifyApplicationApi

**Custom LWC for viewing all Application info for an Internal Account:**

 - This is only available on the 'Internal Department' Account layout
 - ![](https://bitbucket.org/eoghanmcm/recruitmentapplication/raw/a967a26f667121cb4de03e1fb14823cce6bb5fc4/Job%20Application%20Viewer.PNG)

**Trigger Framework Created**

 - Please see these classes: TriggerDispatch, TriggerInterface, TriggerSwitchHandler