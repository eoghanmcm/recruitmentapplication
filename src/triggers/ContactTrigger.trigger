/**
 * @description Trigger for Contact. All logic should be handled by ContactTriggerHandler
 */

trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatch.run(new ContactTriggerHandler(), Trigger.operationType);
}