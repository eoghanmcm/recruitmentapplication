/**
 * @description Job_Posting__c trigger
 */

trigger JobPostingTrigger on Job_Posting__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatch.run(new JobPostingTriggerHandler(), Trigger.operationType);
}