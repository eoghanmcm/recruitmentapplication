<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Application_Id</fullName>
        <description>Updates the unique Id of an Applciation</description>
        <field>Application_Unique_identifier__c</field>
        <formula>Applicant__c +  Job_Posting__r.Job_Id__c</formula>
        <name>Update Unique Application Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Unique Application Id</fullName>
        <actions>
            <name>Update_Unique_Application_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates a unique Id for this Application. It is a combination of Account.Id and the associated Job Id. Must be updated if Job Posting or Associated account is updated</description>
        <formula>OR(ISBLANK(Application_Unique_identifier__c), ISCHANGED(Job_Posting__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
