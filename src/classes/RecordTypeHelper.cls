/**
 * @description Statically stores RecordType information for access during Apex transaction
 */

public with sharing class RecordTypeHelper {
    public static final Map<String, Schema.RecordTypeInfo> accountRecordTypeMap = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();
    public static final Id ACCOUNT_APPLICANT_RT_ID = accountRecordTypeMap.get(Constants.ACCOUNT_RT_NAME_APPLICANT).getRecordTypeId();

    public static final Map<String, Schema.RecordTypeInfo> contactRecordTypeMap = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName();
    public static final Id CONTACT_APPLICANT_RT_ID = contactRecordTypeMap.get(Constants.CONTACT_RT_NAME_APPLICANT).getRecordTypeId();
}