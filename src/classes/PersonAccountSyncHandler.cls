/**
 * @description Provides functionality for syncing Person Accounts and their associated contacts
 */

public class PersonAccountSyncHandler {

    /**
     * @description Creates an Applicant Account for any Applicant Contacts that are inserted
     *              Does not create an Applicant Account if one is already set on the Contact
     *
     * @param contact
     */
    public static void createApplicantAccountsForContacts(List<Contact> contact){
        List<Contact> applicationContacts = getApplicationContactsWithNoAccount(contact);
        createApplicationAccounts(applicationContacts);
    }

    /**
     * @description Generates a List of Applicant Contacts with no associated Account
     *
     * @param contacts
     *
    */
    private static List<Contact> getApplicationContactsWithNoAccount(List<Contact> contacts){
        List<Contact> filteredContacts = new List<Contact>();
        for(Contact c : contacts){
            if(c.RecordTypeId == RecordTypeHelper.CONTACT_APPLICANT_RT_ID && c.AccountId == null){
                filteredContacts.add(c);
            }
        }
        return filteredContacts;
    }

    /**
     * Generates an associated Account for each Application Contact
     *
     * @param contacts
     */
    private static void createApplicationAccounts(List<Contact> contacts){
        List<Account> accounts = new List<Account>();
        for(Contact c : contacts){
            if(c.AccountId == null) {
                accounts.add(
                        new Account(
                                Name = getAccountName(c),
                                Applicant_Contact__c = c.Id,
                                RecordTypeId = RecordTypeHelper.ACCOUNT_APPLICANT_RT_ID
                        )
                );
            }
        }

        if(accounts.size() > 0) {
            insert accounts;
            setAccountIdOnApplicationContact(accounts);
        }
    }

    /**
     * @description Updates Contacts with the Application Account created for them
     *
     * @param accounts
     */
    private static void setAccountIdOnApplicationContact(List<Account> accounts){
        List<Contact> contactsToUpdate = new List<Contact>();
        for(Account acc : accounts){
            //Check if Account successfully inserted by checking Id
            if(acc.Id != null && acc.Applicant_Contact__c != null){
                contactsToUpdate.add(
                        new Contact(Id = acc.Applicant_Contact__c, AccountId = acc.Id)
                );
            }
        }

        //Disable Contact Trigger to avoid recursive issues
        ContactTriggerHandler.TRIGGER_DISABLED = true;
        update contactsToUpdate;
        ContactTriggerHandler.TRIGGER_DISABLED = false;
    }

    private static String getAccountName(Contact c){
        if(String.isNotBlank(c.FirstName)){
            return c.FirstName + ' ' + c.LastName;
        }
        return c.LastName;
    }
}