/**
 * @description Interface for Trigger implementation
 */

public interface TriggerInterface {
    void beforeInsert(SObject[] newItems);

    void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);

    void beforeDelete(Map<Id, SObject> oldItems);

    void afterInsert(Map<Id, SObject> newItems);

    void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);

    void afterDelete(Map<Id, SObject> oldItems);

    void afterUndelete(Map<Id, SObject> oldItems);

    // This method needs to be implemented to check whether or not trigger logic should run.
    Boolean isDisabled(String sobjectName);
}