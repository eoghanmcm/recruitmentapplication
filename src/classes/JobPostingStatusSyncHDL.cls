/**
 * @description Responsible for setting all Application__c records belonging to a Job_Posting__c to Inactive when the Parent Job_Posting__c is set as Inactive
 */

public class JobPostingStatusSyncHDL {
    public static void syncJobPostingApplicationStatus(List<Job_Posting__c> jobPostings, Map<Id, Job_Posting__c> oldJobPostings){
        List<Job_Posting__c> inactiveJobPostings = getJobPostingsChangedToInactive(jobPostings, oldJobPostings);
        setRelatedApplicationsToInactive(inactiveJobPostings);
    }

    /**
    * @description returns Job Postings which have just changed to inactive
     */
    private static List<Job_Posting__c> getJobPostingsChangedToInactive(List<Job_Posting__c> jobPostings, Map<Id, Job_Posting__c> oldJobPostingsMap){
        List<Job_Posting__c> filteredJobs = new List<Job_Posting__c>();

        if(oldJobPostingsMap != null) {
            for (Job_Posting__c posting : jobPostings) {
                if(oldJobPostingsMap.containsKey(posting.Id)){
                    Job_Posting__c oldPosting = oldJobPostingsMap.get(posting.Id);

                    if(String.isNotBlank(posting.Status__c) && posting.Status__c.equalsIgnoreCase(Constants.INACTIVE_STATUS) && posting.Status__c != oldPosting.Status__c){
                        filteredJobs.add(posting);
                    }
                }
            }
        }

        return filteredJobs;
    }

    /**
     * @description Sets any Application__c records related to the Job_Posting__c records to inactive status
     *
     * @param inactiveJobPostings
     */
    private static void setRelatedApplicationsToInactive(List<Job_Posting__c> inactiveJobPostings){
        List<Application__c> applicationsToUpdate = getApplications(inactiveJobPostings);

        if(applicationsToUpdate.size() > 0){
            for(Application__c application : applicationsToUpdate){
                application.Status__c = Constants.INACTIVE_STATUS;
            }
        }

        List<Database.SaveResult> results = Database.update(applicationsToUpdate, false);
        DmlHelper.printErrorMessages(results, 'Error Updating Applications in JobPostingStatusSyncHDL');
    }

    private static List<Application__c> getApplications(List<Job_Posting__c> inactiveJobPostings){
        return [SELECT Id FROM Application__c WHERE Job_Posting__c IN: inactiveJobPostings AND Status__c !=: Constants.INACTIVE_STATUS];
    }
}