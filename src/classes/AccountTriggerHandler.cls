/**
 * @description Trigger Handler for the Account object.
                NOTE this class should be logic-less and should pass work to be done to handlers
 */

public class AccountTriggerHandler implements TriggerInterface{
    // Allows unit tests (or other code) to disable this trigger
    public static Boolean TRIGGER_DISABLED = false;

    /**
        @description Returns true if this trigger has been disabled via TRIGGER_DISABLED Boolean or TriggerSwitch__c custom setting
    */
    public Boolean isDisabled(String sobjectType){
        if(TriggerSwitchHandler.isTriggerDisabled(sobjectType)){
            return true;
        }
        //Return here for trigger disabled via code & not custom setting
        return TRIGGER_DISABLED;
    }

    public void beforeInsert(List<SObject> newItems) {}

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {}

    public void afterInsert(Map<Id, SObject> newItems) {
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void afterDelete(Map<Id, SObject> oldItems) {}

    public void afterUndelete(Map<Id, SObject> oldItems) {}
}