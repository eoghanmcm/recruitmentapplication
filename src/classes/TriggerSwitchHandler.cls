/**
 * @description Controls Activation of Trigger via custom setting TriggerSwitch__c
 */

public class TriggerSwitchHandler {
    private static final String ALL_OBJECTS = 'All Objects';

    /**
     * @description Checks custom settings to verify if a Trigger using the Framework is active
     *              A record is inserted automatically for each trigger that fires
     *              A record named 'All Objects' is inserted to provide functionality to disable org-wide triggers
     *
     * @param objectApiName
     *
     * @return
     */
    public static Boolean isTriggerDisabled(String objectApiName){
        List<TriggerSwitch__c> triggerSwitchesToCreate = new List<TriggerSwitch__c>();
        TriggerSwitch__c allObjectsTriggerSwitch = TriggerSwitch__c.getValues(ALL_OBJECTS);
        TriggerSwitch__c currentObjectTriggerSwitch = TriggerSwitch__c.getValues(objectApiName);
        Boolean isDisabled = false;

        //Check are all object triggers disabled
        if(allObjectsTriggerSwitch != null){
            if(allObjectsTriggerSwitch.IsDisabled__c) isDisabled = true;
        }
        else{
            triggerSwitchesToCreate.add(
                    new TriggerSwitch__c(Name=ALL_OBJECTS, IsDisabled__c=false)
            );
        }

        //check if the current executing trigger is disabled
        if(currentObjectTriggerSwitch != null){
            if(currentObjectTriggerSwitch.IsDisabled__c) isDisabled = true;
        }
        else{
            triggerSwitchesToCreate.add(
                    new TriggerSwitch__c(Name=objectApiName, IsDisabled__c=false)
            );
        }

        //Create any TriggerSwitch__c which do not exist
        try {
            if(triggerSwitchesToCreate.size() > 0) insert triggerSwitchesToCreate;
        } catch (Exception e) {
            System.debug('Error adding trigger switches: ' + e.getMessage() + ' ' + e.getStackTraceString());
        }

        return isDisabled;
    }
}