/**
 * @description Class for statically storing Constant values
 */

public class Constants {
    public static final String ACCOUNT_RT_NAME_APPLICANT = 'Applicant';
    public static final String CONTACT_RT_NAME_APPLICANT = 'Applicant';
    public static final String INACTIVE_STATUS = 'Inactive';
    public static final String ACTIVE_STATUS = 'Active';
}