/**
  @Description     : Exposed REST endpoint for creating Applications
 */

@RestResource(UrlMapping='/ApplicationCreation/*')
global with sharing class ShopifyApplicationApi {

    private static final String SUCCESS = '200';
    private static final String ERROR_NULL_APP_INFO = '402';
    private static final String ERROR_CONTACT_MISSING = '403';
    private static final String ERROR_JOB_ID_MISSING = '404';
    private static final String ERROR_APPLICATIONS_MISSING = '405';

    private static final String ERROR_MISSING_REQUIRED_NAME = '406';
    private static final String ERROR_MISSING_REQUIRED_JOB_ID = '407';
    private static final String ERROR_MISSING_REQUIRED_EMAIL = '408';

    /**
     * @description Accepts a JSON List of Application information
     *              Application information is parsed and the required records are created
     *
     *              If an Applicant Contact already exists then the Contacts fields are updated(unique Id is Email Address)
     *
     *              If an Application for a Contact already exists then we do not create a duplicate
     * @param applicationInfo a List of ApplicationWrapper objects.
     *                        Allows REST caller input a List of Applications to be created
     */
    @HttpPost
    global static String doPost(List<ApplicationWrapper> applicationInfo) {
        if(applicationInfo != null){
            String reqFieldsError = getRequiredFieldError(applicationInfo);
            if(String.isNotBlank(reqFieldsError)){
                return reqFieldsError;
            }
            //Set a save point, if anything fails we will roll back the entire operation
            Savepoint sp = Database.setSavepoint();

            Map<String, Contact> emailToContactMap = createApplicantContacts(applicationInfo);
            Map<String, Job_Posting__c> jobIdToJobPostingMap = getExistingJobPostingMap(applicationInfo);
            List<Application__c> applications = generateApplications(emailToContactMap, jobIdToJobPostingMap, applicationInfo);

            String errorMessage = getErrorMessage(applicationInfo, emailToContactMap, jobIdToJobPostingMap, applications);

            if(String.isNotBlank(errorMessage)){
                System.debug('Rolling Back DMLs in ShopifyApplicationApi, error: ' + errorMessage);
                Database.rollback(sp);
                return errorMessage;
            }
        }
        else{
            return ERROR_NULL_APP_INFO;
        }

        return SUCCESS;
    }

    /**
    * @description A Wrapper for JSON Application info
     */
    global class ApplicationWrapper{
        public String firstName;
        public String lastName;
        public String emailAddress;
        public String addressLine1;
        public String addressCity;
        public String addressCountry;
        public String addressPostalCode;
        public String addressProvince;
        public String phoneNumber;
        public String jobId;
    }

    /**
     * @description If the ApplicationWrapper.emailAddress is already associated with an Applicant Contact in the system then the Contact is updated with the latest info
     *              If no Contact exists for the ApplicationWrapper.emailAddress then a new Contact is created
     *
     * @param applicationInfos
     *
     * @return A Map from Email Address to the associated contact Record
     */
    private static Map<String, Contact> createApplicantContacts(List<ApplicationWrapper> applicationInfos){
        Map<String, Contact> emailToContactMap = getExistingContactsByEmailAddress(applicationInfos);
        List<Contact> contactsToUpdate = reSyncExistingContacts(emailToContactMap, applicationInfos);
        List<Contact> contactsToInsert = getContactsToCreate(emailToContactMap.keySet(), applicationInfos);

        List<Database.SaveResult> results  = Database.insert(contactsToInsert, false);
        DmlHelper.printErrorMessages(results, 'Error inserting Contacts in ShopifyApplicationApi');

        results  = Database.update(contactsToUpdate, false);
        DmlHelper.printErrorMessages(results, 'Error updating Contacts in ShopifyApplicationApi');

        List<Contact> allContacts = new List<Contact>(contactsToInsert);
        allContacts.addAll(contactsToUpdate);

        return getEmailToContactMap(allContacts);
    }

    /**
     * @description Returns exisiting Applicant Contacts by Email Address
     *              Email Address is used as a unique identifier
     *
     * @param applicationInfos
     *
     * @return
     */
    private static Map<String, Contact> getExistingContactsByEmailAddress(List<ApplicationWrapper> applicationInfos){
        Map<String, Contact> emailAddressToContactMap = new Map<String, Contact>();
        Set<String> emailAddresses = getEmailAddresses(applicationInfos);

        //SOQL for loop for efficiency see here: https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/langCon_apex_loops_for_SOQL.htm
        for(List<Contact> contacts : [
                SELECT Id, Email, FirstName, LastName, Phone, MailingAddress, AccountId
                FROM Contact
                WHERE Email IN: emailAddresses
                AND RecordTypeId =: RecordTypeHelper.CONTACT_APPLICANT_RT_ID]
        ) {
            for(Contact c : contacts){
                emailAddressToContactMap.put(c.Email.toLowerCase(), c);
            }
        }

        return emailAddressToContactMap;
    }


    /**
     * @description Generates a Set of all email addresses
    */
    private static Set<String> getEmailAddresses(List<ApplicationWrapper> applicationInfos){
        Set<String> emailAddresses = new Set<String>();

        for(ApplicationWrapper appInfo : applicationInfos){
            if(String.isNotBlank(appInfo.emailAddress)){
                emailAddresses.add(appInfo.emailAddress);
            }
        }

        return emailAddresses;
    }

    /**
     * @description ensures all required fields have been populated
     *              This could be made smarter by collecting a list of rows that have errors
    */
    private static String getRequiredFieldError(List<ApplicationWrapper> applicationInfos) {
        Integer rowNumber = 0;

        for (ApplicationWrapper appInfo : applicationInfos) {
            if (String.isBlank(appInfo.emailAddress)){
                return ERROR_MISSING_REQUIRED_EMAIL + ' row: ' + rowNumber;
            }
            if(String.isBlank(appInfo.firstName) || String.isBlank(appInfo.lastName)){
                return ERROR_MISSING_REQUIRED_NAME + ' row: ' + rowNumber;
            }
            if(String.isBlank(appInfo.jobId)){
                return ERROR_MISSING_REQUIRED_JOB_ID + ' row: ' + rowNumber;
            }
            rowNumber++;
        }
        return '';
    }

    /**
     * Generates a List of Contacts to update
     *             Updates to reflect newest values
     *
     * @param emailToContactMap
     * @param appInfos
     */
    private static List<Contact> reSyncExistingContacts(Map<String, Contact> emailToContactMap, List<ApplicationWrapper> appInfos){
        List<Contact> contactsToUpdate = new List<Contact>();
        for(ApplicationWrapper appInfo : appInfos){
            if(appInfo.emailAddress != null && emailToContactMap.containsKey(appInfo.emailAddress.toLowerCase())){
                Contact c = emailToContactMap.get(appInfo.emailAddress.toLowerCase());
                setContactFields(c, appInfo);
                contactsToUpdate.add(c);
            }
        }
        return contactsToUpdate;
    }

    /**
     * @description Sets fields on Account from ApplicationWrapper
     */
    private static void setContactFields(Contact c, ApplicationWrapper appInfo){
        c.FirstName = appInfo.firstName;
        c.LastName =appInfo.lastName;
        c.Phone = appInfo.phoneNumber;
        c.Email = appInfo.emailAddress;
        c.MailingStreet = appInfo.addressLine1;
        c.MailingCity = appInfo.addressCity;
        c.MailingCountry = appInfo.addressCountry;
        c.MailingPostalCode = appInfo.addressPostalCode;
        c.MailingState = appInfo.addressProvince;
    }

    /**
     * @description Generates a new Contact if the ApplicationWrapper.emailAddress is not already associated with an Applicant Contact
     */
    private static List<Contact> getContactsToCreate(Set<String> existingContactEmails, List<ApplicationWrapper> appInfos){
        List<Contact> newContacts = new List<Contact>();

        for(ApplicationWrapper appInfo : appInfos){
            if(!existingContactEmails.contains(appInfo.emailAddress.toLowerCase())){
                Contact c = new Contact();
                setContactFields(c, appInfo);
                c.RecordTypeId = RecordTypeHelper.CONTACT_APPLICANT_RT_ID;
                newContacts.add(c);
            }
        }

        return newContacts;
    }

    /**
     * @description Generates a Map from Email address to Associated Contact
     *              Re-queries the Contacts the retrieve the auto generated AccountId (Via Contact Trigger see ContactTriggerHandler)
     */
    private static Map<String, Contact> getEmailToContactMap(List<Contact> contacts){
        Map<String, Contact> emailToContactMap = new Map<String, Contact>();

        for (List<Contact> contactsQueried : [
                SELECT Id, Email, FirstName, LastName, Phone, MailingAddress, AccountId
                FROM Contact
                WHERE Id IN: contacts
                AND RecordTypeId = :RecordTypeHelper.CONTACT_APPLICANT_RT_ID
        ]) {
            for(Contact c : contactsQueried){
                if (c.Email != null) {
                    emailToContactMap.put(c.Email, c);
                }
            }
        }
        return emailToContactMap;
    }

    /**
     * Retrieves a Map of all the existing Job_Posting__c whose Job_Id__c is referenced in the ApplicationWrapper JSON
     */
    private static Map<String, Job_Posting__c> getExistingJobPostingMap(List<ApplicationWrapper> appInfos){
        Map<String, Job_Posting__c> jobIdToJobPostingMap = new Map<String, Job_Posting__c>();
        Set<String> jobIds = getJobIds(appInfos);

        for(List<Job_Posting__c> jobPostings : [SELECT Id, Job_Id__c FROM Job_Posting__c WHERE Job_Id__c IN: jobIds]){
            for(Job_Posting__c posting : jobPostings){
                if(String.isNotBlank(posting.Job_Id__c)){
                    jobIdToJobPostingMap.put(posting.Job_Id__c, posting);
                }
            }
        }

        return jobIdToJobPostingMap;
    }

    /**
     * @description Generates a set of all Job Ids from the ApplicationWrapper List
     *
     */
    private static Set<String> getJobIds(List<ApplicationWrapper> appInfos){
        Set<String> jobIds = new Set<String>();

        for(ApplicationWrapper appInfo : appInfos){
            if(String.isNotBlank(appInfo.jobId)){
                jobIds.add(appInfo.jobId);
            }
        }

        return jobIds;
    }

    /**
     * @description Creates Application__c records for all ApplicationWrapper in JSON.
     *              Application__c.Application_Unique_identifier__c is used to upsert Application records to avoid duplicates
     *
     * @param emailToContactMap
     * @param jobIdToJobPostingMap
     * @param appInfos
     *
     * @return
     */
    private static List<Application__c> generateApplications(Map<String, Contact> emailToContactMap, Map<String, Job_Posting__c> jobIdToJobPostingMap, List<ApplicationWrapper> appInfos){
        List<Application__c> applications = new List<Application__c>();

        for(ApplicationWrapper appInfo : appInfos){
            if(emailToContactMap.containsKey(appInfo.emailAddress.toLowerCase()) && jobIdToJobPostingMap.containsKey(appInfo.jobId)){
                Contact c = emailToContactMap.get(appInfo.emailAddress.toLowerCase());
                Job_Posting__c jobPosting = jobIdToJobPostingMap.get(appInfo.jobId);

                if(c.AccountId != null){
                    applications.add(
                        new Application__c(
                            Applicant__c = c.AccountId,
                            Job_Posting__c = jobPosting.Id,
                            Application_Unique_identifier__c =  c.AccountId + appInfo.jobId
                        )
                    );
                }
            }
        }

        List<Database.UpsertResult> results = Database.upsert(applications, Application__c.Application_Unique_identifier__c, false);
        DmlHelper.printErrorMessages(results, 'Error Upserting Application__c records in ShopifyApplicationApi');

        return getSuccessfulApps(applications);
    }

    /**
     * @description if an App was successfully inserted by checking the Id
     */
    private static List<Application__c> getSuccessfulApps(List<Application__c> applications){
        List<Application__c> createdApps = new List<Application__c>();

        for(Application__c app : applications){
            if(app.Id != null){
                createdApps.add(app);
            }
        }

        return createdApps;
    }

    /**
     * @description returns an Error String if any information was not successfully created or updated by the API
     *
     * @param appInfos
     * @param emailToContactMap
     * @param jobIdToJobPostingMap
     * @param applications
     *
    */
    private static String getErrorMessage(
            List<ApplicationWrapper> appInfos,
            Map<String, Contact> emailToContactMap,
            Map<String, Job_Posting__c> jobIdToJobPostingMap,
            List<Application__c> applications
    ) {
        String error = '';

        System.debug('appInfos: ' + appInfos);
        System.debug('emailToContactMap: ' + emailToContactMap);
        System.debug('jobIdToJobPostingMap: ' + jobIdToJobPostingMap);
        System.debug('applications: ' + applications);

        //Check for contact Errors
        for(ApplicationWrapper appInfo : appInfos){
            if(!emailToContactMap.keySet().contains(appInfo.emailAddress.toLowerCase())){
                error = error + appInfo.emailAddress + ',';
            }
        }
        if(String.isNotBlank(error)){
            return ERROR_CONTACT_MISSING + ': ' + error.removeEnd(',');
        }

        //Check for Job_Posting__c missing
        for(ApplicationWrapper appInfo : appInfos){
            if(!jobIdToJobPostingMap.keySet().contains(appInfo.jobId)){
                error = error + appInfo.jobId + ',';
            }
        }
        if(String.isNotBlank(error)){
            return ERROR_JOB_ID_MISSING + ': ' + error.removeEnd(',');
        }

        if(applications.size() != appInfos.size()){
            return ERROR_APPLICATIONS_MISSING;
        }

        return '';
    }
}