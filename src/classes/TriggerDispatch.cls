/**
 * @description runs appropriate method on Trigger handler dependant on Trigger context
 */

public class TriggerDispatch {
    /*
      Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
      This method will fire the appropriate methods on the handler depending on the trigger context.
    */
    public static void run(TriggerInterface handler, System.TriggerOperation triggerEvent) {

        String sobjectName = getSobjectType();

        // Check to see if the trigger has been disabled. If it has, return
        if (String.isBlank(sobjectName) || handler.isDisabled(sobjectName)) {
            System.debug('Trigger is disabled via TriggerSwitch__c OR error getting Sobject Name: ' + sobjectName);
            //Return if it's not enabled, stops trigger from executing
            return;
        }

        switch on triggerEvent {
            when BEFORE_INSERT {
                handler.beforeInsert(Trigger.new);
            } when BEFORE_UPDATE {
                handler.beforeUpdate(Trigger.newMap, Trigger.oldMap);
            } when BEFORE_DELETE {
                handler.beforeDelete(Trigger.oldMap);
            } when AFTER_INSERT {
                handler.afterInsert(Trigger.newMap);
            } when AFTER_UPDATE {
                handler.afterUpdate(Trigger.newMap, Trigger.oldMap);
            } when AFTER_DELETE {
                handler.afterDelete(Trigger.oldMap);
            } when AFTER_UNDELETE {
                handler.afterUndelete(Trigger.oldMap);
            }
        }
    }

    private static String getSobjectType(){
        SObjectType triggerType = Trigger.isDelete ? Trigger.old.getSObjectType() : Trigger.new.getSObjectType();
        if(triggerType == null) return '';
        return String.valueOf(triggerType);
    }
}