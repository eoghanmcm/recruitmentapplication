/**
 * @description controller for the LWC applicationViewer
 */

public class ApplicationViewerController {

    private static String JOB_POSTINGS_ACCESS_STRING = 'jobPostings';
    private static String JOB_POSTINGS_APPS_ACCESS_STRING = 'jobApplicationsMap';

    /**
     * @description returns the active Applications for an Account
     *
     */
    @AuraEnabled(Cacheable=true)
    public static LightningResponse getActiveApplications(Id accountId){
        LightningResponse response = new LightningResponse();

        try {
            System.debug('accountId: ' + accountId);
            Set<Id> jobPostingIds = getActiveJobPostingIds(accountId);
            System.debug('jobPostingIds: ' + jobPostingIds);
            List<Job_Posting__c> jobPostings = getActiveJobPostingsAndApplications(jobPostingIds);
            System.debug('jobPostings: ' + jobPostings);

            Map<Id, List<ApplicantInformationWrapper>> jobPostingIdToApplicantInfos = getJobToAssociatedApplicantAccounts(jobPostings);

            response.dataMap.put(JOB_POSTINGS_ACCESS_STRING, jobPostings);
            response.dataMap.put(JOB_POSTINGS_APPS_ACCESS_STRING, jobPostingIdToApplicantInfos);
        } catch (DmlException e) {
            throw new AuraHandledException(e.getDmlMessage(0));
        }catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    /**
     * @description returns the Active Job Posting Ids for an Account
     *
     */
    private static Set<Id> getActiveJobPostingIds(Id accountId){
        Set<Id> jobPostingIds = new Set<Id>();

        for (List<Job_Sponsor__c> jobSponsors : [
                SELECT Id, Job_Posting__c
                FROM Job_Sponsor__c
                WHERE Internal_Department_Account__c = :accountId
                AND Job_Posting__r.Status__c = :Constants.ACTIVE_STATUS
        ]) {
            for (Job_Sponsor__c jobSponsor : jobSponsors) {
                jobPostingIds.add(jobSponsor.Job_Posting__c);
            }
        }

        return jobPostingIds;
    }

    /**
     * Generates a List of all the Active Job Postings for an Account
     * Runs a Child Query to retrieve all Applications__c for each Job_Posting__c
     *
     * @param jobPostingIds
     *
     * @return
     */
    private static List<Job_Posting__c> getActiveJobPostingsAndApplications(Set<Id> jobPostingIds){
        return [
                SELECT Id, Name,  Status__c, Job_Category__c, Job_Title__c, Job_Posting_Link__c, (
                        SELECT Id,
                                Name,
                                Status__c,
                                Applicant__c,
                                Applicant__r.Applicant_First_Name__c,
                                Applicant__r.Applicant_Last_Name__c,
                                Applicant__r.Applicant_Contact__r.Id,
                                Applicant__r.Applicant_Email__c,
                                Applicant__r.Applicant_Name__c
                        FROM Applications__r
                        WHERE Status__c =: Constants.ACTIVE_STATUS
                )
                FROM Job_Posting__c
                WHERE Id IN:jobPostingIds
                AND Status__c = :Constants.ACTIVE_STATUS
        ];
    }

    /**
     * Generates a Map from Job Id to all of the associated Application(and Applicants) living under that job_Posting__c
     *
     * @param jobPostings
     *
     * @return
     */
    private static Map<Id, List<ApplicantInformationWrapper>> getJobToAssociatedApplicantAccounts(List<Job_Posting__c> jobPostings){
        Map<Id, List<ApplicantInformationWrapper>> jobPostingIdToApplicantAccounts = new Map<Id, List<ApplicantInformationWrapper>>();

        for(Job_Posting__c jobPosting : jobPostings){
            List<ApplicantInformationWrapper> applicationsWrappers = new List<ApplicantInformationWrapper>();

            for(Application__c application : jobPosting.Applications__r){
                ApplicantInformationWrapper wrapper = new ApplicantInformationWrapper();
                wrapper.accountId = application.Applicant__c;
                wrapper.applicantName = application.Applicant__r.Applicant_Name__c;
                wrapper.applicantContactId = application.Applicant__r.Applicant_Contact__r.Id;
                wrapper.contactLink = '/' + application.Applicant__r.Applicant_Contact__r.Id;
                wrapper.applicantEmail = application.Applicant__r.Applicant_Email__c;
                wrapper.applicationName = application.Name;
                wrapper.applicationId = application.Id;
                wrapper.applicationLink = '/' + application.Id;
                wrapper.applicationStatus = application.Status__c;


                applicationsWrappers.add(wrapper);
            }

            jobPostingIdToApplicantAccounts.put(jobPosting.Id, applicationsWrappers);
        }
        return jobPostingIdToApplicantAccounts;
    }

    public class ApplicantInformationWrapper{
        @AuraEnabled public Id accountId {get; set;}
        @AuraEnabled public String applicantName {get; set;}
        @AuraEnabled public String applicantContactId {get; set;}
        @AuraEnabled public String contactLink {get; set;}
        @AuraEnabled public String applicantEmail {get; set;}
        @AuraEnabled public String applicationName {get; set;}
        @AuraEnabled public String applicationLink {get; set;}
        @AuraEnabled public String applicationId {get; set;}
        @AuraEnabled public String applicationStatus {get; set;}
    }
}