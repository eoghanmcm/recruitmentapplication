/**
 * @description Provides methods for printing DML error messages
 */

public class DmlHelper {

    /**
     * @description Prints errors for dml operations
     *
     * @param results
     * @param errorMessage allows code to add a unique error message for easy finding in debug logs
     */
    public static void printErrorMessages(List<Database.SaveResult> results, String errorMessage){
        for (Database.SaveResult result : results) {
            if(!result.isSuccess() && result.errors != null) {
                for(Database.Error err : result.getErrors()) {
                    System.debug(errorMessage + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    public static void printErrorMessages(List<Database.UpsertResult> results, String errorMessage){
        for (Database.UpsertResult result : results) {
            if(!result.isSuccess() && result.errors != null) {
                for(Database.Error err : result.getErrors()) {
                    System.debug(errorMessage + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}