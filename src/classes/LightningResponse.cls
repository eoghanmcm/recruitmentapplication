/**
 * @description A generic response for returning data to a lightning component
 */

public class LightningResponse {
    @AuraEnabled
    public Map<String, Object> dataMap {get;set;}
    @AuraEnabled
    public String returnMessage {get;set;}
    @AuraEnabled
    public Boolean isSuccess {get;set;}

    public LightningResponse(){
        dataMap = new Map<String, Object>();
        returnMessage = '';
        isSuccess = true;
    }
}