import { LightningElement, track, api, wire} from 'lwc';
import getActiveApplications from '@salesforce/apex/ApplicationViewerController.getActiveApplications';
import NoDataLabel from '@salesforce/label/c.No_Job_Posting_Data_Available';
import ApplicantInfoLabel from '@salesforce/label/c.Applicant_Information_For_Job';
import AllJobPostingsLabel from '@salesforce/label/c.All_Job_Postings';

const actions = [
    { label: 'Show details', name: 'show_details' }
];

const columns = [
    { label: 'Job Posting Name', fieldName: 'Job_Posting_Link__c' , type: 'url',
      typeAttributes: {
          label:{
              fieldName:'Name'
          },
          target: '_self'
      },
    },
    { label: 'Status', fieldName: 'Status__c' },
    { label: 'Job Category', fieldName: 'Job_Category__c'},
    { label: 'Job Title', fieldName: 'Job_Title__c'},
    {
        type: 'button',
        typeAttributes: {
            name: "show_details",
            title: "Show Details",
            label: "Show Details",
            variant: "brand"
        }
    },
];

const applicationColumns = [
    { label: 'Application Name', fieldName: 'applicationLink', type: 'url',
        typeAttributes: {
            label:{
              fieldName:'applicationName'
            },
            target: '_self'
        },
    },
    { label: 'Applicant Name', fieldName: 'contactLink', type: 'url',
        typeAttributes: {
            label:{
               fieldName:'applicantName'
            },
            target: '_self'
        },
    },
    { label: 'Applicant Email', fieldName: 'applicantEmail', type: 'email'},
];

export default class ApplicationViewerCmp extends LightningElement {
    @api recordId;
    @track isLoading = true;
    @track allActiveApplications;
    @track jobPostingToAllApplications;
    @track columns = columns;
    @track selectedApplications;
    @track applicationColumns = applicationColumns;
    @track selectedJobPostingName;

    label = {
         NoDataLabel,
         ApplicantInfoLabel,
         AllJobPostingsLabel
    };

    async connectedCallback() {
        this.getApplications();
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        switch (actionName) {
            case 'show_details':
                this.showRelatedApplications(row);
                break;
            default:
        }
    }

    showRelatedApplications(row){
        if(this.jobPostingToAllApplications && this.jobPostingToAllApplications[row.Id]){
            this.selectedJobPostingName = this.label.ApplicantInfoLabel + ' ' + row.Name;
            this.selectedApplications = this.jobPostingToAllApplications[row.Id];
        }
    }

    getApplications(){
        getActiveApplications({ accountId: this.recordId})
            .then(result => {
                if(result.isSuccess){
                    if(result.dataMap.jobPostings.length > 0){
                        this.allActiveApplications = result.dataMap.jobPostings;
                    }
                    if(result.dataMap.jobApplicationsMap){
                        this.jobPostingToAllApplications = result.dataMap.jobApplicationsMap;
                    }
                }
                this.isLoading = false;
            })
            .catch(error => {
                //TODO Add error toast
                this.isLoading = false;
                this.error = error;
            });
    }

    get showNoDataMessage(){
        if(!this.isLoading && this.allActiveApplications == undefined){
            return true;
        }
        return false;
    }

    get showDataTable(){
        if(!this.isLoading && this.allActiveApplications){
            return true;
        }
        return false;
    }

    get showApplicationsDataTable(){
        if(!this.isLoading && this.allActiveApplications && this.selectedApplications){
            return true;
        }
        return false;
    }
}